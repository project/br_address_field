<?php

namespace Drupal\br_address_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use GuzzleHttp\Client;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;

/**
 * Plugin implementation of the 'br_address_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "br_address_widget_type",
 *   label = @Translation("Brazilian address"),
 *   field_types = {
 *     "br_address_field_type"
 *   }
 * )
 */
class BrAddressWidgetType extends WidgetBase {

  use MessengerTrait;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'consult_postal_code' => 1,
      'show_address_container' => 1,
      'required_postal_code' => 1,
      'required_thoroughfare' => 1,
      'required_street_complement' => 0,
      'required_number' => 1,
      'required_neighborhood' => 1,
      'required_city' => 1,
      'required_state' => 1,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['consult_postal_code'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fill address'),
      '#description' => $this->t('Auto fill address by postal code field.'),
      '#default_value' => $this->getSetting('consult_postal_code'),
      '#required' => FALSE,
    ];

    $form['show_address_container'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show address container'),
      '#description' => $this->t('Show a container around the address fields.'),
      '#default_value' => $this->getSetting('show_address_container'),
      '#required' => FALSE,
    ];

    $form['required_fields'] = [
      '#type' => 'label',
      '#title' => $this->t('Required fields'),
    ];

    $form['required_postal_code'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Postal code'),
      '#default_value' => $this->getSetting('required_postal_code'),
    ];

    $form['required_thoroughfare'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Thoroughfare'),
      '#default_value' => $this->getSetting('required_thoroughfare'),
    ];

    $form['required_number'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Number'),
      '#default_value' => $this->getSetting('required_number'),
    ];

    $form['required_street_complement'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Complement'),
      '#default_value' => $this->getSetting('required_street_complement'),
    ];

    $form['required_neighborhood'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Neighborhood'),
      '#default_value' => $this->getSetting('required_neighborhood'),
    ];

    $form['required_city'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('City'),
      '#default_value' => $this->getSetting('required_city'),
    ];

    $form['required_state'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('State'),
      '#default_value' => $this->getSetting('required_state'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Auto fill address: @consult_postal_code', ['@consult_postal_code' => $this->getSetting('consult_postal_code') == 1 ? $this->t('True') : $this->t('False')]);
    $summary[] = $this->t('Show address container: @show_address_container', ['@show_address_container' => $this->getSetting('show_address_container') == 1 ? $this->t('True') : $this->t('False')]);
    $summary[] = $this->t('Postal code required: @required_postal_code', ['@required_postal_code' => $this->getSetting('required_postal_code') == 1 ? $this->t('True') : $this->t('False')]);
    $summary[] = $this->t('Thoroughfare required: @required_thoroughfare', ['@required_thoroughfare' => $this->getSetting('required_thoroughfare') == 1 ? $this->t('True') : $this->t('False')]);
    $summary[] = $this->t('Number required: @required_number', ['@required_number' => $this->getSetting('required_number') == 1 ? $this->t('True') : $this->t('False')]);
    $summary[] = $this->t('Complement required: @required_street_complement', ['@required_street_complement' => $this->getSetting('required_street_complement') == 1 ? $this->t('True') : $this->t('False')]);
    $summary[] = $this->t('Neighborhood required: @required_neighborhood', ['@required_neighborhood' => $this->getSetting('required_neighborhood') == 1 ? $this->t('True') : $this->t('False')]);
    $summary[] = $this->t('City required: @required_city', ['@required_city' => $this->getSetting('required_city') == 1 ? $this->t('True') : $this->t('False')]);
    $summary[] = $this->t('State required: @required_state', ['@required_state' => $this->getSetting('required_state') == 1 ? $this->t('True') : $this->t('False')]);

    return $summary;
  }

  /**
   * Gets the initial values for the widget.
   *
   * This is a replacement for the disabled default values functionality.
   *
   * @see address_form_field_config_edit_form_alter()
   *
   * @return array
   *   The initial values, keyed by property.
   */
  protected function getInitialValues() {
    $initial_values = [
      'postal_code' => '',
      'thoroughfare' => '',
      'number' => '',
      'street_complement' => '',
      'neighborhood' => '',
      'city' => '',
      'state' => '',
    ];

    return $initial_values;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];
    $default_value_input = !in_array('default_value_input', $form['#parents']);
    $value = $item->isEmpty() ? $this->getInitialValues() : $item->toArray();

    $required_postal_code = $default_value_input && $this->getSetting('required_postal_code');
    $required_thoroughfare = $default_value_input && $this->getSetting('required_thoroughfare');
    $required_number = $default_value_input && $this->getSetting('required_number');
    $required_street_complement = $default_value_input && $this->getSetting('required_street_complement');
    $required_neighborhood = $default_value_input && $this->getSetting('required_neighborhood');
    $required_city = $default_value_input && $this->getSetting('required_city');
    $required_state = $default_value_input && $this->getSetting('required_state');

    $states = [
      'AC' => 'Acre',
      'AL' => 'Alagoas',
      'AP' => 'Amapá',
      'AM' => 'Amazonas',
      'BA' => 'Bahia',
      'CE' => 'Ceará',
      'DF' => 'Distrito Federal',
      'ES' => 'Espírito Santo',
      'GO' => 'Goiás',
      'MA' => 'Maranhão',
      'MT' => 'Mato Grosso',
      'MS' => 'Mato Grosso do Sul',
      'MG' => 'Minas Gerais',
      'PA' => 'Pará',
      'PB' => 'Paraíba',
      'PR' => 'Paraná',
      'PE' => 'Pernambuco',
      'PI' => 'Piauí',
      'RJ' => 'Rio de Janeiro',
      'RN' => 'Rio Grande do Norte',
      'RS' => 'Rio Grande do Sul',
      'RO' => 'Rondônia',
      'RR' => 'Roraima',
      'SC' => 'Santa Catarina',
      'SP' => 'São Paulo',
      'SE' => 'Sergipe',
      'TO' => 'Tocantins',
    ];

    $show_address_container = (boolean) $this->getSetting('show_address_container');
    $consult_postal_code = (boolean) $this->getSetting('consult_postal_code');

    if ($show_address_container == TRUE) {
      $element += [
        '#type' => 'details',
        '#open' => TRUE,
        '#attributes' => [
          'id' => 'address-container',
        ],
      ];
    }
    else {
      $element += [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'id' => 'address-container',
        ],
      ];
    }

    $element['#attached']['library'][] = 'br_address_field/theme';

    $element['postal_code'] = [
      '#type' => 'textfield',
      '#default_value' => $value['postal_code'],
      '#required' => $required_postal_code,
      '#consult_postal_code' => $consult_postal_code,
      '#title' => $this->t('Postal code'),
      '#maxlength' => 10,
    ];

    if ($consult_postal_code) {
      $element['postal_code'] += [
        '#ajax' => [
          'callback' => [$this, 'ajaxConsultZip'],
          'event' => 'change',
          'wrapper' => 'address-container',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Verifying entry...'),
          ],
        ],
      ];
    }

    $element['thoroughfare'] = [
      '#type' => 'textfield',
      '#default_value' => $value['thoroughfare'],
      '#required' => $required_thoroughfare,
      '#title' => $this->t('Thoroughfare'),
      '#attributes' => [
        'id' => 'thoroughfare',
      ],
    ];

    $element['number'] = [
      '#type' => 'textfield',
      '#default_value' => $value['number'],
      '#required' => $required_number,
      '#title' => $this->t('Number'),
      '#maxlength' => 10,
      '#attributes' => [
        'id' => 'street-number',
      ],
    ];

    $element['street_complement'] = [
      '#type' => 'textfield',
      '#default_value' => $value['street_complement'],
      '#required' => $required_street_complement,
      '#title' => $this->t('Complement'),
      '#attributes' => [
        'id' => 'street-complement',
      ],
    ];

    $element['neighborhood_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'neighborhood-wrapper',
      ],
    ];

    $element['neighborhood'] = [
      '#type' => 'textfield',
      '#default_value' => $value['neighborhood'],
      '#required' => $required_neighborhood,
      '#title' => $this->t('Neighborhood'),
      '#attributes' => [
        'id' => 'neighborhood',
      ],
    ];

    $element['city_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'city-wrapper',
      ],
    ];

    $element['city'] = [
      '#type' => 'textfield',
      '#default_value' => $value['city'],
      '#required' => $required_city,
      '#title' => $this->t('City'),
      '#attributes' => [
        'id' => 'city',
      ],
    ];

    $element['state_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'state-wrapper',
      ],
    ];

    $state = $value['state'] ? $value['state'] : 'AC';

    $element['state'] = [
      '#type' => 'select',
      '#options' => $states,
      '#default_value' => $state,
      '#required' => $required_state,
      '#title' => $this->t('State'),
      '#attributes' => [
        'id' => 'state',
      ],
    ];

    return $element;
  }

  /**
   * Call the function that consumes the web service.
   *
   * @param array $form
   *   A form that will be modified.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The values of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The modified AjaxResponse.
   */
  public function ajaxConsultZip(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $parent_keys = $form_state->getTriggeringElement()['#array_parents'];
    $delta = $parent_keys[2];
    $parent_field_name = $parent_keys[0];
    $form_state_values = $form_state->getValues();

    // Retrieve the postal code based on parent keys.
    $elements = $this->createArray($parent_keys, $form_state_values);
    $postal_code = NestedArray::getValue($elements, $parent_keys);
    $postal_code = preg_replace('/[^0-9\-]/', '', $postal_code);
    // Call your web service function and get the new values.
    $consulta_cep_response = $this->consultZip($postal_code);

    if (!isset($consulta_cep_response->erro)) {

      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[$delta][postal_code]\"]", 'val', [$consulta_cep_response->cep]));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[$delta][thoroughfare]\"]", 'val', [$consulta_cep_response->logradouro]));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[$delta][number]\"]", 'val', ['']));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[$delta][street_complement]\"]", 'val', ['']));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[$delta][neighborhood]\"]", 'val', [$consulta_cep_response->bairro]));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[$delta][city]\"]", 'val', [$consulta_cep_response->localidade]));
      $ajax_response->addCommand(new InvokeCommand("select[name=\"{$parent_field_name}[$delta][state]\"]", 'val', [$consulta_cep_response->uf]));
      $ajax_response->addCommand(new InvokeCommand("select[name=\"{$parent_field_name}[$delta][state]\"]", 'change'));

    }
    else {

      $this->messenger()->addError($this->t('Postal code not found.'));

      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[0][postal_code]\"]", 'val', ['']));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[0][thoroughfare]\"]", 'val', ['']));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[0][number]\"]", 'val', ['']));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[0][street_complement]\"]", 'val', ['']));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[0][neighborhood]\"]", 'val', ['']));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[0][city]\"]", 'val', ['']));
      $ajax_response->addCommand(new InvokeCommand("input[name=\"{$parent_field_name}[0][state]\"]", 'val', ['']));
      $ajax_response->addCommand(new InvokeCommand("select[name=\"{$parent_field_name}[0][state]\"]", 'change'));

    }

    return $ajax_response;
  }

  /**
   * Creates an deep array with the keys informed on $parente_keys.
   *
   * @param array $parent_keys
   *   Parent keys.
   * @param array $form_state_values
   *   Form State Values.
   * @param array $created_array
   *   Created Array.
   *
   * @return array
   *   Return array.
   */
  private function createArray($parent_keys, $form_state_values, $created_array = []) {
    $key = array_shift($parent_keys);
    if (!isset($form_state_values[$key])) {
      $postal_code = reset($form_state_values);
      $created_array[$key][$parent_keys[0]] = $postal_code;
    }
    else {
      $created_array[$key] = $this->createArray($parent_keys, $form_state_values[$key]);
    }
    return $created_array;
  }

  /**
   * Consume viacep webservice.
   *
   * @param int $zip
   *   The postal code to consult.
   *
   * @return mixed
   *   Street, Neighborhood, City and state of the postal code.
   */
  public function consultZip($zip) {
    $client = new Client(["http://viacep.com.br/ws/" . $zip . "/json/"]);
    $result = $client->request('get', "http://viacep.com.br/ws/" . $zip . "/json/", ['Accept' => 'application/json']);
    $output = $result->getBody()->getContents();
    $address = json_decode($output);

    return $address;
  }

}
